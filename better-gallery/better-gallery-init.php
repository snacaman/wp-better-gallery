<?php

add_option('better-gallery-viewer', 'lightbox');
add_option('better-gallery-display-caption', 0);

add_option('better-gallery-lightbox-fit-to-screen', 1);
add_option('better-gallery-lightbox-display-title', 1);
add_option('better-gallery-lightbox-navi-bar-top', 0);
add_option('better-gallery-lightbox-slide-navi-bar', 0);
add_option('better-gallery-lightbox-resize-speed', 300);

add_option('better-gallery-fancybox-padding', 10);
add_option('better-gallery-fancybox-imageScale', 'true');
add_option('better-gallery-fancybox-zoomOpacity', 'false');
add_option('better-gallery-fancybox-zoomSpeedIn', 0);
add_option('better-gallery-fancybox-zoomSpeedOut', 0);
add_option('better-gallery-fancybox-zoomSpeedChange', 300);
add_option('better-gallery-fancybox-easingIn', 'swing');
add_option('better-gallery-fancybox-easingOut', 'swing');
add_option('better-gallery-fancybox-easingChange', 'swing');
add_option('better-gallery-fancybox-frameWidth', 425);
add_option('better-gallery-fancybox-frameHeight', 355);
add_option('better-gallery-fancybox-overlayShow', 'false');
add_option('better-gallery-fancybox-overlayOpacity', 0.3);
add_option('better-gallery-fancybox-hideOnContentClick', 'true');		

?>
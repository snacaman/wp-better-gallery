<div id="better-gallery-options" class="wrap">
<div class="icon32" id="icon-options-general"><br />
</div>
<h2><?php _e('Better Gallery Settings', 'better-gallery') ?></h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie massa et justo pretium quis dictum dui imperdiet. Quisque dictum nisl non mi scelerisque vel vehicula elit bibendum. Nulla facilisi. Nunc venenatis sapien quis eros sodales vel tempor mauris ullamcorper.</p>

<form method="post" action="options.php"><?php wp_nonce_field('update-options'); ?>

<table class="form-table">
	<colgroup>
		<col style="width: 250px;" />
		<col />
	</colgroup>
	<tr valign="top">
		<th scope="row"><?php _e('Choose image viewer plugin', 'better-gallery'); ?></th>
		<td><select name="better-gallery-viewer" style="width: 120px;">
			<option value="" <?php echo get_option('better-gallery-viewer') == '' ? ' selected="selected"' : ''; ?>><?php _e('None', 'better-gallery'); ?></option>
			<option value="lightbox" <?php echo get_option('better-gallery-viewer') == 'lightbox' ? ' selected="selected"' : ''; ?>>Lightbox</option>
			<option value="fancybox" <?php echo get_option('better-gallery-viewer') == 'fancybox' ? ' selected="selected"' : ''; ?>>Fancybox</option>
		</select></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Display thumbnails caption', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-display-caption" value="0" /> <input class="cb" type="checkbox" name="better-gallery-display-caption" value="1" <?php echo get_option('better-gallery-display-caption') == '1' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Item fixed width', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fixed-width" value="0" /> <input class="cb" type="checkbox" name="better-gallery-fixed-width" value="1" <?php echo get_option('better-gallery-fixed-width') == '1' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Item fixed height', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fixed-height" value="0" /> <input class="cb" type="checkbox" name="better-gallery-fixed-height" value="1" <?php echo get_option('better-gallery-fixed-height') == '1' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
</table>

<h3><?php _e('Lightbox viewer options', 'better-gallery') ?></h3>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie massa et justo pretium quis dictum dui imperdiet. Quisque dictum nisl non mi scelerisque vel vehicula elit bibendum. Nulla facilisi. Nunc venenatis sapien quis eros sodales vel tempor mauris ullamcorper.</p>

<table class="form-table">
	<colgroup>
		<col style="width: 250px;" />
		<col />
	</colgroup>
	<tr valign="top">
		<th scope="row"><?php _e('Fit large images to screen size', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-lightbox-fitToScreen" value="false" /> <input class="cb" type="checkbox" name="better-gallery-lightbox-fitToScreen" value="true" <?php echo get_option('better-gallery-lightbox-fitToScreen') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Display title', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-lightbox-displayTitle" value="false" /> <input class="cb" type="checkbox" name="better-gallery-lightbox-displayTitle" value="true" <?php echo get_option('better-gallery-lightbox-displayTitle') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Display navi bar on top', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-lightbox-navbarOnTop" value="false" /> <input class="cb" type="checkbox" name="better-gallery-lightbox-navbarOnTop" value="true" <?php echo get_option('better-gallery-lightbox-navbarOnTop') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Slide navi bar', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-lightbox-slideNavBar" value="false" /> <input class="cb" type="checkbox" name="better-gallery-lightbox-slideNavBar" value="true" <?php echo get_option('better-gallery-lightbox-slideNavBar') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Resize animation speed', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-lightbox-resizeSpeed" value="<?php echo get_option('better-gallery-lightbox-resizeSpeed'); ?>" style="width: 50px;" /> <span class="setting-description"><?php _e('milliseconds', 'better-gallery'); ?></span></td>
	</tr>
</table>

<h3><?php _e('Fancybox viewer options', 'better-gallery') ?></h3>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie massa et justo pretium quis dictum dui imperdiet. Quisque dictum nisl non mi scelerisque vel vehicula elit bibendum. Nulla facilisi. Nunc venenatis sapien quis eros sodales vel tempor mauris ullamcorper.</p>

<table class="form-table">
	<colgroup>
		<col style="width: 250px;" />
		<col />
	</colgroup>
	<tr valign="top">
		<th scope="row"><?php _e('Padding around content', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-padding" value="<?php echo get_option('better-gallery-fancybox-padding'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Scale images to fit in viewport ', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fancybox-imageScale" value="false" /> <input class="cb" type="checkbox" name="better-gallery-fancybox-imageScale" value="true" <?php echo get_option('better-gallery-fancybox-imageScale') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Change content transparency when animating', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fancybox-zoomOpacity" value="false" /> <input class="cb" type="checkbox" name="better-gallery-fancybox-zoomOpacity" value="true" <?php echo get_option('better-gallery-fancybox-zoomOpacity') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Speed of the zooming-in animation', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-zoomSpeedIn" value="<?php echo get_option('better-gallery-fancybox-zoomSpeedIn'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Speed of the zooming-out animation', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-zoomSpeedOut" value="<?php echo get_option('better-gallery-fancybox-zoomSpeedOut'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Speed of the animation when changing gallery items', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-zoomSpeedChange" value="<?php echo get_option('better-gallery-fancybox-zoomSpeedChange'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Easing in effect', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-easingIn" value="<?php echo get_option('better-gallery-fancybox-easingIn'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Easing out effect', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-easingOut" value="<?php echo get_option('better-gallery-fancybox-easingOut'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Easing change efect', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-easingChange" value="<?php echo get_option('better-gallery-fancybox-easingChange'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Default dimensions for iframed and inline content', 'better-gallery'); ?></th>
		<td><input type="text" name="better-gallery-fancybox-frameWidth" value="<?php echo get_option('better-gallery-fancybox-frameWidth'); ?>" style="width: 50px;" /> x <input type="text" name="better-gallery-fancybox-frameHeight" value="<?php echo get_option('better-gallery-fancybox-frameHeight'); ?>" style="width: 50px;" /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Display overlay', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fancybox-overlayShow" value="false" /> <input class="cb" type="checkbox" name="better-gallery-fancybox-overlayShow" value="true" <?php echo get_option('better-gallery-fancybox-overlayShow') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Opacity of overlay', 'better-gallery'); ?></th>
		<td><select name="better-gallery-fancybox-overlayOpacity" style="width: 80px;">
		<?php
		$bgfOverlayOpacity = get_option('better-gallery-fancybox-overlayOpacity');
		for($x=1; $x<=10; $x++) : ?>
			<option value="<?php echo ($x/10); ?>" <?php echo $bgfOverlayOpacity == ($x/10) ? ' selected="selected"' : ''; ?>><?php echo ($x*10) ?>%</option>
			<?php endfor; ?>
		</select></td>
	</tr>
	<tr valign="top">
		<th scope="row"><?php _e('Hide FancyBox when cliked on opened item', 'better-gallery'); ?></th>
		<td><input type="hidden" name="better-gallery-fancybox-hideOnContentClick" value="false" /> <input class="cb" type="checkbox" name="better-gallery-fancybox-hideOnContentClick" value="true" <?php echo get_option('better-gallery-fancybox-hideOnContentClick') == 'true' ? ' checked="checked"' : ''; ?> /></td>
	</tr>
</table>

<input type="hidden" name="action" value="update" /> <input type="hidden" name="page_options" value="better-gallery-viewer, better-gallery-display-caption, better-gallery-fixed-width, better-gallery-fixed-height, <?php echo $this->UpdateOptions()?>" />

<p class="submit"><input type="submit" name="Submit" value="<?php _e('Save Changes', 'better-gallery') ?>" /></p>
</form>
</div>

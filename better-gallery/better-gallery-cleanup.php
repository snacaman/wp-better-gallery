<?php

delete_option('better-gallery-viewer');
delete_option('better-gallery-display-caption');

delete_option('better-gallery-lightbox-fit-to-screen');
delete_option('better-gallery-lightbox-display-title');
delete_option('better-gallery-lightbox-navi-bar-top');
delete_option('better-gallery-lightbox-slide-navi-bar');
delete_option('better-gallery-lightbox-resize-speed');

delete_option('better-gallery-fancybox-show-overlay');
delete_option('better-gallery-fancybox-overlay-opacity');
delete_option('better-gallery-fancybox-zoomin-speed');
delete_option('better-gallery-fancybox-zoomout-speed');

delete_option('better-gallery-fancybox-padding');
delete_option('better-gallery-fancybox-imageScale');
delete_option('better-gallery-fancybox-zoomOpacity');
delete_option('better-gallery-fancybox-zoomSpeedIn');
delete_option('better-gallery-fancybox-zoomSpeedOut');
delete_option('better-gallery-fancybox-zoomSpeedChange');
delete_option('better-gallery-fancybox-easingIn');
delete_option('better-gallery-fancybox-easingOut');
delete_option('better-gallery-fancybox-easingChange');
delete_option('better-gallery-fancybox-frameWidth');
delete_option('better-gallery-fancybox-frameHeight');
delete_option('better-gallery-fancybox-overlayShow');
delete_option('better-gallery-fancybox-overlayOpacity');
delete_option('better-gallery-fancybox-hideOnContentClick');		

?>
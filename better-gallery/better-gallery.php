<?php

/*
 * Plugin Name: Better Gallery
 * Version: 0.1.1
 * Description: Improves the built-in Wordpress gallery and brings it W3C's xHTML compliant. Plugin also utilizes some javascript viewers like jQuery lightbox.
 * Author: Janusz Krupski
 * Author URI: http://janusz.spinbit.eu/
 * Plugin URI: http://janusz.spinbit.eu/
 *
 * Copyright (C) 2009 Janusz Krupski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class BetterGalleryPlugin {

	protected $_viewer = '';
	protected $_version = '0.1.1';
	protected $_compatible = true;
	protected $_viewersOptions;

	public function __construct() {
		$this->_Init();
	}

	protected function _Init() {

		load_plugin_textdomain('better-gallery', false, 'better-gallery/languages');

		if(false) {
			$this->_compatible = false;
			add_action('admin_notices', array($this, 'VersionWarning'));
			return false;
		}

		/*
		 * Viewers options and it's defaults
		 *
		 * array(
		 *	'ViewerName' => array(
		 *		'OptionName' => array('DefaultValue', NeedsQuote, FromDatabase))
		 *	)
		 */
		$this->_viewersOptions = array(
			'lightbox' => array(
				'fitToScreen' => array('true', false, true),
				'borderSize' => array('1', false, false),
				'resizeSpeed' => array('300', false, true),
				'displayTitle' => array('true', false, true),
				'navbarOnTop' => array('false', false, true),
				'slideNavBar' => array('false', false, true),
				'displayHelp' => array('false', false, false),
				'fileLoadingImage' => array('/wp-content/plugins/better-gallery/viewers/lightbox/images/loading.gif', true, false),
				'fileBottomNavCloseImage' => array('/wp-content/plugins/better-gallery/viewers/lightbox/images/closelabel.gif', true, false),
			),
			'fancybox' => array(
				'padding' => array('10', false, true),
				'imageScale' => array('true', false, true),
				'zoomOpacity' => array('false', false, true),
				'zoomSpeedIn' => array('0', false, true),
				'zoomSpeedOut' => array('0', false, true),
				'zoomSpeedChange' => array('300', false, true),
				'easingIn' => array('swing', true, true),
				'easingOut' => array('swing', true, true),
				'easingChange' => array('swing', true, true),
				'frameWidth' => array('425', false, true),
				'frameHeight' => array('355', false, true),
				'overlayShow' => array('false', false, true),
				'overlayOpacity' => array('0.3', false, true),
				'hideOnContentClick' => array('true', false, true)
			)
		);

		$this->_viewer = get_option('better-gallery-viewer', '');

		if(is_admin()) {
			add_filter('plugin_action_links', array($this, 'ActionLinks'), 10, 2);
			add_action('admin_menu', array($this, 'AddOptionsPage'));
			wp_enqueue_style('better-gallery-admin-css', '/wp-content/plugins/better-gallery/css/admin.css', false, $this->_version, 'screen');
		}
		else {
			add_filter('post_gallery', array($this, 'PostGallery'), 10, 2);

			// Better Gallery default style
			wp_enqueue_style('better-gallery', '/wp-content/plugins/better-gallery/css/better-gallery.css', false, $this->_version, 'screen');

			// jQuery Lightbox
			if($this->_viewer == 'lightbox') {
				wp_enqueue_style('lightbox-css', '/wp-content/plugins/better-gallery/viewers/lightbox/css/lightbox.css', false, $this->_version, 'screen');
				wp_enqueue_style('lightbox-custom-css', '/wp-content/plugins/better-gallery/css/lightbox-custom.css', false, $this->_version, 'screen');
				wp_enqueue_script('jquery-lightbox-js', '/wp-content/plugins/better-gallery/viewers/lightbox/jquery.lightbox.js', array('jquery'), $this->_version);
			}

			// jQuery Fancybox
			if($this->_viewer == 'fancybox') {
				wp_enqueue_style('fancybox-css', '/wp-content/plugins/better-gallery/viewers/fancybox/jquery.fancybox.css', false, $this->_version, 'screen');
				wp_enqueue_script('jquery-fancybox-js', '/wp-content/plugins/better-gallery/viewers/fancybox/jquery.fancybox-1.2.1.pack.js', array('jquery'), $this->_version);
			}
		}
	}

	protected function _ViewerScript($id = null) {

		$options = '';
		foreach ($this->_viewersOptions[$this->_viewer] as $option => $def) {
			$quote = ($def[1]) ? '\'' : '';
			if($def[2]) {
				$value = get_option('better-gallery-' . $this->_viewer . '-' . $option);
			}
			else {
				$value = $def[0];
			}
			$options .= $option . ' : ' . $quote . $value . $quote . ', ';
			unset($quote);
		}
		$options = rtrim($options, ' ,');
		$options .= '';

		if($this->_viewer == 'fancybox') {
			return "anchors{$id}.fancybox({" . $options . "});";
		}

		if($this->_viewer == 'lightbox') {

			$lbprevimg = __('Previous image', 'better-gallery');
			$lbnextimg = __('Next image', 'better-gallery');
			$lbprev = __('Previous', 'better-gallery');
			$lbnext = __('Next', 'better-gallery');
			$lbclose = __('Close', 'better-gallery');
			$lbimage = __('Image', 'better-gallery');
			$lbof = __('of', 'better-gallery');

			$options .= ", strings : {
				help: 'Help text here...',
				prevLinkTitle : '{$lbprevimg}',
				nextLinkTitle : '{$lbnextimg}',
				prevLinkText : '{$lbprev}',
				nextLinkText : '{$lbnext}',
				closeTitle : '{$lbclose}',
				image : '{$lbimage} ',
				of : ' {$lbof} '
			}";

			return "anchors{$id}.lightbox({" . $options . "});";
		}
	}

	public function UpdateOptions() {
		$updateOptions = '';
		foreach($this->_viewersOptions as $viewer => $options) {
			foreach($options as $option => $def) {
				if($def[2]) {
					$updateOptions .= 'better-gallery-' . $viewer . '-' . $option . ', ';
				}
			}
		}
		return rtrim($updateOptions, ', ');
	}

	public function PostGallery($output, $attr) {

		global $post;
		$displayCaption = get_option('better-gallery-display-caption');

		// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
		if(isset($attr['orderby'])) {
			$attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
			if(!$attr['orderby'])
			unset($attr['orderby']);
		}

		extract(shortcode_atts(array(
			'id'         => $post->ID,
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'itemtag'    => 'dl',
			'icontag'    => 'dt',
			'captiontag' => 'dd',
			'columns'    => 3,
			'size'       => 'thumbnail',
		), $attr));

		$link2file = false;
		if(isset($attr['link']) && 'file' == $attr['link']) {
			$link2file = true;
		}

		if($link2file) {
			$vs = $this->_ViewerScript($post->ID);
		}

		$id = intval($id);
		$attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

		if(empty($attachments))
		return '';

		if (is_feed()) {
			$output = "\n";
			foreach($attachments as $id => $attachment)
			$output .= wp_get_attachment_link($id, $size, true) . "\n";
			return $output;
		}

		$listtag = tag_escape($listtag);
		$itemtag = tag_escape($itemtag);
		$captiontag = tag_escape($captiontag);
		$columns = intval($columns);
		$itemwidth = $columns > 0 ? floor(100/$columns) : 100;

		$i = 0;
		$output = "<div class='gallery gallery{$post->ID}'>\n";

		if($i == 0 && $link2file) {
			$output .= "<script type='text/javascript'>
			jQuery(function($) {
				var anchors{$post->ID} = $('div.gallery{$post->ID} a');
				anchors{$post->ID}.attr('rel', 'gallery{$post->ID}');
				{$vs}
			});
			</script>";
		}

		$fixedW = get_option('better-gallery-fixed-width');
		$fixedH = get_option('better-gallery-fixed-height');

		$thumbSizeW = ($fixedW) ? get_option('thumbnail_size_w') : false;
		$thumbSizeH = ($fixedH) ? get_option('thumbnail_size_h') : false;

		$itemStyle = '';
		if($thumbSizeW || $thumbSizeH) {
			$itemStyle .= ' style="';
			$itemStyle .= ($thumbSizeW) ? "width: {$thumbSizeW}px; " : '';
			$itemStyle .= ($thumbSizeH) ? "height: {$thumbSizeH}px; " : '';
			$itemStyle .= '"';
		}

		foreach($attachments as $id => $attachment) {

			$link = ($link2file) ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
			$imgSrc = wp_get_attachment_image_src($id, $size, false);

			if($columns > 0 && $i % $columns == 0) {
				$output .= '<div class="gallery-row">';
			}

			$output .= "
			<{$itemtag} class='gallery-item";
			if ($columns > 0 && $i % $columns == 0) {
				$output .= " first";
			}
			$output .= "'>";

			$output .= "
			<{$icontag} class='gallery-icon'{$itemStyle}>
			$link
			</{$icontag}>";

			if($displayCaption && $captiontag && trim($attachment->post_excerpt)) {
				$output .= "
				<{$captiontag} class='gallery-caption'>
				<small>{$attachment->post_excerpt}</small>
				</{$captiontag}>";
			}

			$output .= "</{$itemtag}>";

			if(($columns > 0 && ++$i % $columns == 0) || $i == count($attachments))
			$output .= '</div>';
		}

		$output .= "</div>";

		return $output;
	}

	public function AttachmentPage() {

		if($this->_viewer != '') {
			$out = "<script type=\"text/javascript\">
				jQuery(function($) {
					var anchors = $('.attachment a');"
					. $this->_ViewerScript() .
				"});
			</script>";
			echo $out;
		}
	}

	/* @TDOD: Unused? Remove?
	public function PostPage() {

		if($this->_viewer != '') {
			$out = "<script type=\"text/javascript\">
				jQuery(function($) {
					var anchors = $('.wp-caption a, a.box');"
					. $this->_ViewerScript() .
				"});
			</script>";
			echo $out;
		}
	} */

	public function AddOptionsPage() {
		add_options_page(__('Better Gallery Settings', 'better-gallery'), 'Better Gallery', 8, 'better-gallery', array($this, 'OptionsPage'));
	}

	public function OptionsPage() {
		//require('better-gallery-cleanup.php');
		//require('better-gallery-init.php');
		require('better-gallery-options.php');
	}

	public function ActionLinks($links, $file) {
		if(dirname($file) == 'better-gallery') {
			array_push($links, '<a href="options-general.php?page=better-gallery">' . __('Settings') . '</a>');
		}
		return $links;
	}

	public function VersionWarning() {
		echo '<div class="error"><p>Better Gallery Plugin can\'t work with this WordPress version!</p></div>';
	}
}

$BetterGalleryPlugin = new BetterGalleryPlugin();

/*
$attachments = get_children( array(
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'numberposts' => -1,
	'post_status' => null,
	'post_parent' => null,
	'output' => 'object',
));

echo '<pre>';
// print_r($attachments);

foreach ($attachments as $attachment) {
	$fullSizeImage = get_attached_file($attachment->ID);
	$fileName = basename($fullSizeImage);
	$dirName = dirname($fullSizeImage);

	preg_match('/^(.+)\.([^.]+)$/', $fileName, $m);
	$fileNamePart = $m[1];
	$fileExtPart = $m[2];

	$relatedImages = glob($dirName . '/' . $fileNamePart . '-*x*');

	echo 'unlink related to: ' . $fileName . '<br>';
	flush();
	foreach($relatedImages as $item) {
		unlink($item);
	}

	wp_update_attachment_metadata(
		$attachment->ID,
		wp_generate_attachment_metadata($attachment->ID, $fullSizeImage)
	);

	echo 'regen from: ' . $fileName . '<br>';
	flush();

//	echo $dirName . '<br>';
//	echo $fileName . '<br>';
//	print_r($relatedImages);

}
*/

?>